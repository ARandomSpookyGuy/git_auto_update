source ~/git_auto_update/variables.sh

clear



for path in $(cat $paths_txt); do
	cd $path 
	path_short=$(echo $path | awk -F' |/' 'NF>1{print $NF}')

	has_new=$(git status -s | grep "??")

	has_new_modified=$(for item in $has_new; do echo -n "${item} "; done)

	line_count=$((line_count+1))

	if [[ $has_new == '' ]]; then
		echo "${line_count}) ${path_short}"
	else	
		echo -en "${line_count}) ${path_short} | *New Files Found | ${has_new_modified} \n"
	fi
done

echo -en "\nFolder: "; read choice 

line_break

line_count=0

edit(){
	line_count=0

	while true; do
		echo -n "Add: "; read file
		if [[ $file == '' ]];then echo "Exiting"; break; else	
			git add -f $file

			for item in $(cat .gitignore); do 
				line_count=$((line_count+1))
				if [[ $item == $file ]];then
					sed -i "$line_count"'d' .gitignore					
				fi
			done
			git commit -am "${file} pushed to repository."
			echo "$file added to local repository."

		fi
	done
}
delete(){
	while true; do
		echo -n "Remove: "; read file 
		if [[ $file == '' ]];then echo "Exiting"; break; else
			#Remove file from repository and add it to .gitignore
			git rm --cached $file
		 	echo $file  >> .gitignore
			echo "$file removed from local repository"
		fi
	done
}

for path in $(cat $paths_txt); do
	line_count=$((line_count+1))
	if [[ $line_count == $choice ]];then 

		clear	
		cd $path

		untracked=$(git status -s  | grep ?? | awk '{print $2}')
		all=$(git ls-files)
		ignored=$(cat .gitignore)

		line_break

		header "Untracked Files"
		for item in $untracked; do echo $item; done

		line_break_small		

		header "Ignored files:"
		for item in $ignored; do echo $item; done		


		line_break_small		

		header "Repository:"
		for item in $all; do echo $item; done

	
		line_break

		echo -e "1) Add file\n2) Remove file\n"
		echo -n "Choice: ";read edit_type

		if [[ $edit_type == "1" ]];then edit; elif [[ $edit_type == '2' ]]; then
			delete
		fi

	fi
done
