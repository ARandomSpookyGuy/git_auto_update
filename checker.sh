#!/bin/sh

source ~/git_auto_update/variables.sh

clear
if [[ $1 == '' ]]; then

	for path in $(cat $paths_txt); do 
		cd $path 
		path_short=$(echo $path | awk -F' |/' 'NF>1{print $NF}' )
		has_updates=$(git status | grep "Changes not staged for commit") 
		if [[ $has_updates != '' ]]; then 
			git commit -am update > /dev/null
			if [[ $(cat $update_txt | wc -l) == 0 ]] ;then echo $path >> $update_txt 
			else
				has_item=$(cat $update_txt | grep "${path}")
				if [[ $has_item == '' ]]; then
					echo $path >> ~/git_auto_update/updates.txt
				fi
			fi
		fi
	done
elif [[ $1 == '1' ]]; then
	for path in $(cat $update_txt); do 
		path_short=$(echo $path | awk -F' |/' 'NF>1{print $NF}' )

		cd $path

		echo -n "Update ${path_short}? [<y>/n]";read choice

		if [[ $choice == '' || $choice == 'y' ]]; then 
			git commit -am update

			pass -c /website/github/key > /dev/null
			git push origin -f $git_branch 
			number=$(cat $update_txt | grep -n $path)
			sed -i "${number:0:1}"'d' $update_txt

		elif [[ $choice == 'n' ]]; then
			echo "${path_short} has been skipped." 
		fi
	done
fi


