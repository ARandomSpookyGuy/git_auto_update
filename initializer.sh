#!/bin/sh 

source ~/git_auto_update/variables.sh

if [[ $1 == '' ]];then echo "Please attach remote origin.";exit; fi


git init
git branch -M $git_branch 
git remote add origin $git_origin

touch .gitignore
git add .gitignore


if [[ $user_current_path == '/home/ghostuser/dotfiles' ]]; then
for file in $user_current_path/.*; do 
	file_cut=$(echo $file | awk -F' |/' 'NF>1{print $NF}' )
	echo -n "Add $file_cut to Local Repo? [<y>/n]"; read choice
	case $choice in 
		'n') echo $file_cut >> $user_current_path/.gitignore ;;
		'N') echo $file_cut >> $user_current_path/.gitignore ;;
		'No') echo $file_cut >> $user_current_path/.gitignore ;;
		'no') echo $file_cut >> $user_current_path/.gitignore ;;
		*) git add $file_cut ;;
	esac

done
fi

for file in $user_current_path/*; do 
	file_cut=$(echo $file | awk -F' |/' 'NF>1{print $NF}' )
	echo -n "Add $file_cut to Local Repo? [<y>/n]"; read choice
	case $choice in 
		'n') echo $file_cut >> $user_current_path/.gitignore ;;
		'N') echo $file_cut >> $user_current_path/.gitignore ;;
		'No') echo $file_cut >> $user_current_path/.gitignore ;;
		'no') echo $file_cut >> $user_current_path/.gitignore ;;
		*) git add $file_cut ;;
	esac

done


if [[ $(cat $paths_txt | wc -l) == 0 ]]; then echo $user_current_path >> $paths_txt; fi

for path in $(cat $paths_txt); do 
	if [[ $path != $user_current_path ]];then
		echo $user_current_path >> $paths_txt
		break
	fi
done
