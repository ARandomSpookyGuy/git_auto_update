#!/bin/sh 

source ~/git_auto_update/variables.sh


for path in $(cat $paths_txt); do
	line_count=$((line_count+1))
	path_cut=$(echo $path | awk -F' |/' 'NF>1{print $NF}' )
	echo "$line_count) $path_cut"
done

echo -en "Remove: "; read choice


line_count=0
for path in $(cat $paths_txt); do
	line_count=$((line_count+1))
	
	if [[ $line_count == $choice ]];then 
		update_path=$path

		sed -i "$line_count"'d' $paths_txt 
		echo $path
		cd $path ; sudo rm -r .git*
	fi
done

line_count=0
for path in $(cat $update_txt); do 
	line_count=$((line_count+1))
	if [[ $path == $update_path ]];then
		sed -i "${line_count}"'d' $update_txt
	fi

done
