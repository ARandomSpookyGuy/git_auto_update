#!/bin/sh

paths_txt=~/git_auto_update/git.txt
update_txt=~/git_auto_update/updates.txt
user_current_path=$PWD

#User Identification
git_branch=main
git_origin_base=https://arandomspookyguy@github.com/arandomspookyguy
git_origin_target=$1
git_origin=$git_origin_base/$git_origin_target

#Colors
RED='\033[0;31m'
BLUE='\033[4;34m'
BN='\033[0;34m'
CYAN='\033[1;36m'
NC='\033[0m'

has_commits=""
cancelled_commit=""

line_count=0

#Array
declare -a update_array


line_break(){
		echo -e "\n${RED}|-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_|${NC}\n"
}

line_break_small(){
		echo -e "\n${BN}|_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-|${NC}"
}


header(){
	echo -e "\n${CYAN}${1}${NC}\n"
}
