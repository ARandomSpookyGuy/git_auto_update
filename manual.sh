#!/bin/sh
source ~/git_auto_update/variables.sh

for path in $(cat $paths_txt); do 
	line_count=$((line_count+1))
	echo -e "${line_count}) ${path}"
done
echo "$((line_count+1))) All"
echo -n "Choice: "; read choice
last=$(($line_count+1))
echo $last
line_count=0

for path in $(cat $paths_txt); do 
	path_cut=$(echo $path | awk -F' |/' 'NF>1{print $NF}' )
	line_count=$((line_count+1))

	if [[ $line_count == $choice ]];then 
		cd $path
		clear

		line_break

		echo -en "${BLUE}Finalizing Updates\n${NC}"

		path_capitalized=$(echo $path_cut | awk '{print toupper($1)}')
		
		echo -en "\nPush ${RED}$path_capitalized${NC} to git? [<y>/n]" ; read commit

		git status -s | grep M

		if [[ $commit == 'y' || $commit = '' ]];then 
			echo -e "\n${BLUE}${item_cut}"
			line_break

			git commit -am update

			#cd $item
			pass -c /website/github/key > /dev/null
			git push -f origin $git_branch 

		fi
	fi
done

if [[ $choice == $last || $choice == '' ]]; then 
	for path in $paths_txt; do 
		cd $path 
		git commit -am update
		if [[ $(cat $update_txt | wc -l) == 0 ]]; then echo $item >> ~/git_auto_update/updates.txt; else 

			for path in $(cat $update_txt); do
				update_txt_cut=$(echo $item | awk -F' |/' 'NF>1{print $NF}' )
				if [[ $item != $path ]]; then
					echo $item >> ~/git_auto_update/updates.txt
				fi
			done
			
			echo "${item} not pushed."
		fi

		
	done

fi
